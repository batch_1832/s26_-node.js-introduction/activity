
//3. The questions are as follows:
/*

- What directive is used by Node.js in loading the modules it needs?
Answer: require
- What Node.js module contains a method for server creation?
Answer: HTTP
- What is the method of the http object responsible for creating a server using Node.js?
Answer: createServer
- What method of the response object allows us to set status codes and content types?
Answer: writeHead
- Where will console.log() output its contents when run in Node.js?
Answer: Terminal
- What property of the request object contains the address's endpoint?
Answer: response

*/

// 4. Create an index.js file inside of the activity folder.

// 5. Import the http module using the required directive.

const http = require("http");

// 6. Create a variable port and assign it with the value of 3000.

const port = 3000;

// 7. Create a server using the createServer method that will listen in to the port provided above.

http.createServer(function(req, res){
	if(req.url == "/login"){
		res.writeHead(200, {"Content-Text":"text/plain"})
		res.end("Welcome to the log in page")}


		else {
		res.writeHead(404, {"Content-Text":"text/plain"})
		res.end("I'm sorry the page that you are looking for cannot be found.")
		}
}).listen(port);

// 8. Console log in the terminal a message when the server is successfully running.
console.log(`Server running at local host:${port}`);

// 9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
 	// - see if() line 33
// 10. Access the login route to test if it’s working as intended.

// 11. Create a condition for any other routes that will return an error message.
	// - see else{} line 38